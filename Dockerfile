FROM alpine:3.9.2

RUN apk add nodejs npm git \
 && npm install -g gitbook-cli \
 && gitbook --version
